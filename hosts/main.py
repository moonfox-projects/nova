import json
from json import JSONDecodeError


def get_group_names(filename: str) -> list[str]:
    with open(filename) as f:
        try:
            content = json.load(f)
        except JSONDecodeError:
            content = []

    return [group["name"] for group in content if group.get("name", None)]

