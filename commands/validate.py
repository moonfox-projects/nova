import json
import glob
from json import JSONDecodeError
from os.path import isfile

from constants import CONFIG_PATH
from error.error import invalid_format_error, invalid_file_extension_error, abort, malformed_file_error


def get_all_filenames() -> list[str]:
    return [f for f in glob.glob(f"{CONFIG_PATH}/**/*", recursive=True) if isfile(f)]


def has_invalid_file_extension(filenames: list[str]) -> bool:
    invalid_extension = [filename for filename in filenames if not filename.endswith(".conf")]
    for filename in invalid_extension:
        invalid_file_extension_error(filename)

    return bool(invalid_extension)


def has_invalid_file_format_single(filename: str) -> bool:
    with open(filename, "r") as f:
        try:
            content = json.load(f)
        except JSONDecodeError as e:
            if e.msg != "Expecting value":
                malformed_file_error(filename)
            content = []

    invalid = type(content) is not list

    if invalid:
        invalid_format_error(filename)

    return invalid


def has_invalid_file_format(filenames: list[str]) -> bool:
    return bool([filename for filename in filenames if has_invalid_file_format_single(filename)])


def has_duplicate_group_names(filenames: list[str]) -> bool:
    ...


def validate():
    filenames = get_all_filenames()
    has_invalid_extensions = has_invalid_file_extension(filenames)
    has_invalid_format = has_invalid_file_format(filenames)

    if has_invalid_extensions or has_invalid_format:
        abort()
