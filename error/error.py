def malformed_file_error(filename: str) -> None:
    print(f"Config file should not be malformed (Found {filename})")


def invalid_format_error(filename: str) -> None:
    print(f"Config file root must be a list (Found {filename})")


def invalid_file_extension_error(filename: str) -> None:
    print(f"Config file should end with \".conf\" (Found {filename})")


def duplicate_group_name_error(group: str) -> None:
    print(f"Group names should be unique (Found {group})")


def abort():
    print("", end="", flush=True)
    exit(1)
