import sys

from commands.validate import validate

args = sys.argv[1:]


def main():
    if "--validate" in args:
        validate()


if __name__ == "__main__":
    main()
